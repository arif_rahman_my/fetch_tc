# Fetch

Fetch on Gazebo 9

## How to test

1. Clone the `r0-kinetic` branch
2. `$ roslaunch fetch_manipulation_gazebo main.launch`
3. `$ gzclient`
4. `$ roslaunch test_moveit_config fetch_planning_execution.launch` <-- for MoveIt!
5. `$ rosrun teleop_twist_keyboard teleop_twist_keyboard.py` <-- for teleop

